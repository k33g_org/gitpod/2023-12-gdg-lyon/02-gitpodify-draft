FROM gitpod/workspace-full:latest

USER gitpod

RUN <<EOF
brew install bat
brew install exa
EOF
