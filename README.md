# 02-gitpodify-draft

```bash
gp init
```

## `.gitpod.yml`

- update the `command` section (`.gitpod.yml`)

```yaml
command: |
    echo 'start script'
    brew install bat
    brew install exa
```
 - restart

## `.gitpod.dockerfile`

- move `.gitpod.yml` to root
- change the `image` section of `.gitpod.yml`:

```yaml
image:
 file: .gitpod.dockerfile
```

Hello
